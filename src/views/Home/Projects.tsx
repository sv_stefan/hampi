import React, { useEffect } from 'react';
import { useInView } from 'react-intersection-observer';
import HeadingPrimary from '../../atoms/HeadinePrimary';
import styled from 'styled-components';
import ProjectsDesktop from './ProjectsDesktop';
import { useIntersectionObserver } from '../../reusable/useIntersectionObserver';
import ProjectsMobile from './ProjectsMobile';

const Projects = () => {
  const [ref, inView] = useInView({
    threshold: 1,
  });

  const { setIntersection } = useIntersectionObserver();

  useEffect(() => {
    if (inView) {
      setIntersection('#projects');
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [inView]);

  return (
    <div ref={ref}>
      <Container id="projects">
        <HeadingPrimary fontAlign='right'>Projects</HeadingPrimary>
        <ProjectsDesktop />
        <ProjectsMobile /> 
      </Container>
    </div>
  );
};

const Container = styled.div`
  margin: 110px 0 0 0;
`;

export default Projects;
