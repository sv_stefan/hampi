import React from "react";
import styled from "styled-components";
import { useHistory } from "react-router-dom";
import ProjectCard from "../../components/ProjectCard/ProjectCard";
import suztainProjectImg from "../../img/suztain.jpg";
import otherProjectImg from "../../img/high-paw-project.jpg";
import rockcommerceProjectImg from "../../img/rockcommerceproject.jpg";
import whyserProjectImg from "../../img/whyser.jpg";

export default () => {
  const history = useHistory();
  const handleRouteChange = (projectNumber: string) =>
    history.push(`/projects/${projectNumber}`);

  return (
    <Desktop>
      <ProjectWrapper>
        <FlexRowContainer>
          <ProjectCard
            projectNumber="01"
            buttonName="Read case study"
            title="Corporate identity system"
            text="Corporate identity for tech start-up Whyser. The company offers an online SaaS application that enables users to develop transparent and agile strategies."
            margin="0 38px 0 0"
          />
          <ProjectCardImage
            onClick={() => handleRouteChange("01")}
            width="428px"
            height="276px"
            src={whyserProjectImg}
            alt="suztain-project-img"
          />
        </FlexRowContainer>
        <FlexRowContainer>
          <FlexColumnContainer>
            <ProjectCardImage
              onClick={() => handleRouteChange("02")}
              width="427px"
              height="427px"
              src={suztainProjectImg}
              alt="suztain-project-img"
              style={{ marginBottom: "38px", marginTop: "30px" }}
            />
            <ProjectCard
              projectNumber="04"
              buttonName="Read case study"
              title="Rockcommerce design system"
              text="B2B e-commerce solution for ROCKWOOL Group, a world leader in stone wool solutions."
              margin="0"
            />
          </FlexColumnContainer>
          <div
            style={{
              marginLeft: "35px",
              marginTop: "30px",
              alignSelf: "flex-end",
            }}
          >
            <ProjectCard
              projectNumber="02"
              buttonName="Read case study"
              title="Visual identity"
              text="Rebrand project for company Suztain, an online store which offers selection of sustainable products."
              margin="0 0 28px 0"
            />
            <FlexRowContainer>
              <ProjectCardImage
                onClick={() => handleRouteChange("03")}
                width="196px"
                height="194px"
                style={{ marginRight: "37px" }}
                src={otherProjectImg}
                alt="other-project-img"
              />
              <ProjectCard
                projectNumber="03"
                buttonName="View projects"
                title="Collection of smaller projects"
                margin="0"
              />
            </FlexRowContainer>
            <ProjectCardImage
              onClick={() => handleRouteChange("04")}
              width="428px"
              height="225px"
              style={{ marginTop: "36px" }}
              src={rockcommerceProjectImg}
              alt="rockcommerce-project-img"
            />
          </div>
        </FlexRowContainer>
      </ProjectWrapper>
    </Desktop>
  );
};

const ProjectCardImage = styled.img`
  transition: all 250ms;

  &:hover {
    transform: scale(1.02);
    box-shadow: 0 10px 14px 0 rgba(0, 0, 0, 0.09);
    cursor: pointer;
  }
`;

const ProjectWrapper = styled.div``;

const Desktop = styled.div`
  display: none;

  @media screen and (min-width: 930px) {
    display: block;
  }
`;

const FlexRowContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const FlexColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
`;
