import React from "react";
import ProjectCard from "../../components/ProjectCard/ProjectCard";
import styled from "styled-components";
import suztainProjectImg from "../../img/suztain.jpg";
import otherProjectImg from "../../img/high-paw-project.jpg";
import rockcommerceProjectImg from "../../img/rockcommerceproject.jpg";
import whyserProjectImg from "../../img/whyser.jpg";

const ProjectsMobile = () => (
  <MobileView>
    <img
      width="100%"
      height="100%"
      src={whyserProjectImg}
      alt="whyser-project-img"
    />
    <ProjectCard
      projectNumber="01"
      title="Corporate identity system"
      buttonName="Read case study"
      text="Corporate identity for tech start-up Whyser. The company offers an online SaaS application that enables users to develop transparent and agile strategies."
      margin="20px 0 40px 0"
    />
    <img
      width="100%"
      height="100%"
      src={suztainProjectImg}
      alt="suztain-project-img"
    />
    <ProjectCard
      projectNumber="02"
      title="Visual identity"
      buttonName="Read case study"
      text="Rebrand project for company Suztain, an online store which offers selection of sustainable products."
      margin="20px 0 40px 0"
    />
    <img width="100%" height="100%" src={otherProjectImg} alt="high-paw-img" />
    <ProjectCard
      projectNumber="03"
      title="Collection of smaller projects"
      buttonName="View projects"
      margin="20px 0 40px 0"
    />
    <img
      width="100%"
      height="100%"
      src={rockcommerceProjectImg}
      alt="suztain-project-img"
    />
    <ProjectCard
      projectNumber="04"
      title="Rockcommerce design system"
      buttonName="Read case study"
      text="B2B e-commerce solution for ROCKWOOL Group, a world leader in stone wool solutions."
      margin="20px 0 40px 0"
    />
  </MobileView>
);

const MobileView = styled.div`
  display: block;

  @media screen and (min-width: 930px) {
    display: none;
  }
`;

export default ProjectsMobile;
