import React from "react";
import Masonry, { ResponsiveMasonry } from "react-responsive-masonry";
import HeadingSecondary from "../../atoms/HeadingSecondary";
import Margin from "../../atoms/Margin";
import Paragraph from "../../atoms/Paragraph";
import Photo1 from "../../img/photography/Photography_section_1.jpg";
import Photo2 from "../../img/photography/Photography_section_2.jpg";
import Photo3 from "../../img/photography/Photography_section_3.jpg";
import Photo4 from "../../img/photography/Photography_section_4.jpg";
import Photo5 from "../../img/photography/Photography_section_5.jpg";
import Photo6 from "../../img/photography/Photography_section_6.jpg";
import Photo7 from "../../img/photography/Photography_section_7.jpg";
import Photo8 from "../../img/photography/Photography_section_8.jpg";
import Zoom from "react-medium-image-zoom";

const Photography = () => (
  <Margin top={170} bottom={0}>
    <HeadingSecondary>Photography</HeadingSecondary>
    <Paragraph>
      In my free time, I also enjoy taking photos, my inspiration comes from my
      travels.
    </Paragraph>
    <Paragraph>
      If you are interested to see more of photography work please visit my{" "}
      <a href="https://unsplash.com/@m_hampi" style={{ color: "#ff8962" }}>
        Unsplash profile.
      </a>
    </Paragraph>
    <Margin top={30}>
      <ResponsiveMasonry columnsCountBreakPoints={{ 350: 1, 750: 2, 900: 3 }}>
        <Masonry gutter={"30px"}>
          <Zoom>
            <img style={{ width: "100%" }} alt="Photo4" src={Photo4} />
          </Zoom>
          <Zoom>
            <img style={{ width: "100%" }} alt="Photo5" src={Photo5} />
          </Zoom>
          <Zoom>
            <img style={{ width: "100%" }} alt="Photo1" src={Photo1} />
          </Zoom>
          <Zoom>
            <img style={{ width: "100%" }} alt="Photo8" src={Photo8} />
          </Zoom>
          <Zoom>
            <img style={{ width: "100%" }} alt="Photo7" src={Photo7} />
          </Zoom>
          <Zoom>
            <img style={{ width: "100%" }} alt="Photo2" src={Photo2} />
          </Zoom>
          <Zoom>
            <img style={{ width: "100%" }} alt="Photo6" src={Photo6} />
          </Zoom>
          <Zoom>
            <img style={{ width: "100%" }} alt="Photo3" src={Photo3} />
          </Zoom>
        </Masonry>
      </ResponsiveMasonry>
    </Margin>
  </Margin>
);

export default Photography;
