import React, { useEffect } from "react";
import styled from "styled-components";
import HeadingPrimary from "../../atoms/HeadinePrimary";
import { useInView } from "react-intersection-observer";
import { FlexRow, FlexColumn, FlexEnd } from "../../components/FlexBox";
import Paragraph from "../../atoms/Paragraph";
import michaelaProfilePhoto from "../../img/hampiprofilephoto.jpg";
import HeadingSecondary from "../../atoms/HeadingSecondary";
import Margin from "../../atoms/Margin";
import MaxWidthContainer from "../../atoms/MaxWidthContainer";
import { useIntersectionObserver } from "../../reusable/useIntersectionObserver";

const AboutMeSection = () => {
  const [ref, inView] = useInView({
    threshold: 1,
  });

  const { setIntersection } = useIntersectionObserver();

  useEffect(() => {
    if (inView) {
      setIntersection("#about");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [inView]);

  return (
    <div ref={ref} id="about">
      <Margin top={171}>
        <MobileView>
          <Margin bottom={10}>
            <HeadingPrimary fontAlign="left">About me</HeadingPrimary>
          </Margin>
          <Margin bottom={15}>
            <Paragraph>
              My name is Michaela Hampachelova, I’m 27 years old from Slovakia.
            </Paragraph>
          </Margin>
          <Margin bottom={40}>
            <img
              width="100%"
              alt="michaela-profile"
              src={michaelaProfilePhoto}
            />
          </Margin>

          <HeadingSecondary>Skills</HeadingSecondary>
          <Paragraph>
            My background includes designing from web-based projects to print
            advertising. I’ve been working on large-scale design systems using
            Atomic design methodology. I’m highly skilled in creating dynamic
            and responsive content with Sketch, Figma, and Adobe XD. I’m
            experienced with Abstract and Principle and advanced in Photoshop,
            Illustrator, and InDesign. I have an understanding of HTML/CSS and
            JavaScript.
          </Paragraph>

          <Margin top={40}>
            <HeadingSecondary>Capabilities</HeadingSecondary>
          </Margin>
          <Paragraph>
            Website design, Graphic design, Brand development & Rebranding, Logo
            & Identity system, Editorial design, Information architecture,
            Wireframing & Prototypes.
          </Paragraph>

          <Margin top={40}>
            <HeadingSecondary>Clients</HeadingSecondary>
          </Margin>

          <Paragraph>
          Eli Lilly, Novo Nordisk, ROCKWOOL Group, Roche, LEO Pharma, GE Healthcare, MSD, and Biogen.
          </Paragraph>
        </MobileView>

        <DesktopView>
          <FlexRow>
            <img
              width="311px"
              alt="michaela-profile"
              src={michaelaProfilePhoto}
            />
            <FlexEnd>
              <Margin left={34}>
                <FlexColumn>
                  <FlexColumn>
                    <HeadingPrimary fontAlign="left">About me</HeadingPrimary>
                    <Paragraph>
                      My name is Michaela Hampachelova, I’m 27 years old from
                      Slovakia.
                    </Paragraph>
                  </FlexColumn>
                  <Margin top={34}>
                    <FlexColumn>
                      <HeadingSecondary>Skills</HeadingSecondary>
                      <Paragraph>
                        My background includes designing from web-based projects
                        to print advertising. I’ve been working on large-scale
                        design systems using Atomic design methodology. I’m
                        highly skilled in creating dynamic and responsive
                        content with Sketch, Figma, and Adobe XD. I’m
                        experienced with Abstract and Principle and advanced in
                        Photoshop, Illustrator, and InDesign. I have an
                        understanding of HTML/CSS and JavaScript.
                      </Paragraph>
                    </FlexColumn>
                  </Margin>
                </FlexColumn>
              </Margin>
            </FlexEnd>
          </FlexRow>
          <Margin top={34}>
            <FlexRow>
              <MaxWidthContainer maxWidth={311}>
                <FlexColumn>
                  <HeadingSecondary>Clients</HeadingSecondary>
                  <Paragraph>
                  Eli Lilly, Novo Nordisk, ROCKWOOL Group, Roche, LEO Pharma, GE Healthcare, MSD, and Biogen.
                  </Paragraph>
                </FlexColumn>
              </MaxWidthContainer>
              <Margin left={34}>
                <FlexColumn>
                  <HeadingSecondary>Capabilities</HeadingSecondary>
                  <Paragraph>
                    Website design, Graphic design, Brand development &
                    Rebranding, Logo & Identity system, Editorial design,
                    Information architecture, Wireframing & Prototypes.
                  </Paragraph>
                </FlexColumn>
              </Margin>
            </FlexRow>
          </Margin>
        </DesktopView>
      </Margin>
    </div>
  );
};

export default AboutMeSection;

const MobileView = styled.div`
  display: block;

  @media screen and (min-width: 1200px) {
    display: none;
  }
`;

const DesktopView = styled.div`
  display: none;
  @media screen and (min-width: 1200px) {
    display: block;
  }
`;
