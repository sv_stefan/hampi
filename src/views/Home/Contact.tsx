import React, { useEffect } from "react";
import styled from "styled-components";
import { FlexColumn } from "../../components/FlexBox";
import MaxWidthContainer from "../../atoms/MaxWidthContainer";
import HeadingSecondary from "../../atoms/HeadingSecondary";
import { ExternalLink } from "../../atoms/Link";
import Paragraph from "../../atoms/Paragraph";
import Margin from "../../atoms/Margin";
import defaultFont from "../../styled/fonts";
import SectionBreak from "../../atoms/SectionBreak";
import { useInView } from "react-intersection-observer";
import { useIntersectionObserver } from "../../reusable/useIntersectionObserver";

const Contact = () => {
  const [ref, inView] = useInView({
    threshold: 1,
    rootMargin: "300px",
  });

  const { setIntersection } = useIntersectionObserver();

  useEffect(() => {
    if (inView) {
      setIntersection("#contact");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [inView]);

  return (
    <div ref={ref} id="contact">
      <Margin top={180} bottom={0}>
        <BackgroundContainer>
          <BackgroundWrapper>
            <Container>
              <MaxWidthContainer maxWidth={314}>
                <FlexColumn>
                  <HeadingSecondary color={"#FEF6FF"}>
                    Get in touch
                  </HeadingSecondary>
                  <Paragraph color={"#FEF6FF"}>
                    For business inquiries or collaborations, please contact me
                    here:
                  </Paragraph>
                  <Paragraph color={"#FEF6FF"}>
                    <i>michaelahampachelova@gmail.com</i>
                  </Paragraph>
                  <Margin top={22}>
                    <Paragraph color={"#FEF6FF"}>
                      <b>Based in Copenhagen</b>
                    </Paragraph>
                  </Margin>
                </FlexColumn>
              </MaxWidthContainer>
              <ConnectContainer>
                <HeadingSecondary color={"#FEF6FF"}>Connect</HeadingSecondary>
                <ExternalLink
                  target="_blank"
                  href="https://www.linkedin.com/in/mhampi/"
                >
                  Linkedin
                </ExternalLink>

                <ExternalLink
                  target="_blank"
                  href="https://www.behance.net/hampi"
                >
                  Behance
                </ExternalLink>

                <ExternalLink
                  target="_blank"
                  href="https://unsplash.com/@m_hampi"
                >
                  Unsplash
                </ExternalLink>
              </ConnectContainer>
            </Container>
            <Margin top={42} bottom={12}>
              <SectionBreak color={"#FEF6FF"} />
            </Margin>
            <Footer>© 2021 Michaela Hampachelova</Footer>
          </BackgroundWrapper>
        </BackgroundContainer>
      </Margin>
    </div>
  );
};

const BackgroundContainer = styled.div`
  background: #180643;
  padding: 116px 20px 32px 20px;
`;

const BackgroundWrapper = styled.div`
  max-width: 890px;
  margin: 0 auto;
`;

const Footer = styled.footer`
  ${defaultFont}
  font-size: 12px;
  color: #fef6ff;
  letter-spacing: 0;
  text-align: right;
  line-height: 19px;
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  @media screen and (min-width: 930px) {
    flex-direction: row;
  }
`;

const ConnectContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: 40px 0 0 0;

  @media screen and (min-width: 930px) {
    margin: 0 0 0 34px;
  }
`;

export default Contact;
