import styled from 'styled-components';
import defaultFont from '../../styled/fonts';

const Experiences = styled.div``;

const StyledParagraph = styled.p`
	font-size: 14px;
	font-weight: 400;	
`;

export const ExperienceHeadline = styled.h2`

`;

export const ExperienceSection = styled.div`
	${defaultFont}

	display: flex;
	flex-direction: row;
	border-bottom: 1px solid #180643;
	border-top: 1px solid #180643;
	padding: 20px 0;
	margin: -1px 0 0 0;
`

export const ExperienceDate = styled(StyledParagraph)`
	max-width: 316px;
	width: 100%;
`

export const ExperienceOccupation = styled(StyledParagraph)`
	width: 100%;
`

export const ExperienceOrganization = styled(StyledParagraph)`
	width: 100%;
	text-align: right;
`

export default Experiences;