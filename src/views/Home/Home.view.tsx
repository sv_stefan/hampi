import React from "react";
import styled from "styled-components";
import HeroHome from "../../components/Hero/HeroHome";
import { ContainerWrapper } from "../../styled/ContainerWrapper";
import Projects from "./Projects";
import AboutMeSection from "./AboutMe";
import Margin from "../../atoms/Margin";
import Experiences, {
  ExperienceDate,
  ExperienceOccupation,
  ExperienceOrganization,
  ExperienceSection,
} from "./Experiences";
import Photography from "./Photography";
import HeadingSecondary from "../../atoms/HeadingSecondary";

const Home = () => (
  <Margin top={190} bottom={0}>
    <Container>
      <HeroHome />
      <Projects />
      <AboutMeSection />
      <Margin top={42} bottom={0}>
        <Experiences>
          <Margin top={100} bottom={25}>
            <HeadingSecondary>Work experience</HeadingSecondary>
          </Margin>
          <ExperienceSection>
            <ExperienceDate>Sep 2018 — Dec 2020</ExperienceDate>
            <ExperienceOccupation>Junior digital designer</ExperienceOccupation>
            <ExperienceOrganization>
              Vertic A/S - Digital agency
            </ExperienceOrganization>
          </ExperienceSection>
          <ExperienceSection>
            <ExperienceDate>Jun 2020 — Aug 2020</ExperienceDate>
            <ExperienceOccupation>Digital design intern</ExperienceOccupation>
            <ExperienceOrganization>
              Whyser - Tech start-up
            </ExperienceOrganization>
          </ExperienceSection>
          <ExperienceSection>
            <ExperienceDate>Dec 2017 — Feb 2018</ExperienceDate>
            <ExperienceOccupation>
              Freelance graphic designer
            </ExperienceOccupation>
            <ExperienceOrganization></ExperienceOrganization>
          </ExperienceSection>
        </Experiences>
      </Margin>
      <Margin top={104} bottom={42}>
        <Experiences>
          <Margin top={0} bottom={25}>
            <HeadingSecondary>Education</HeadingSecondary>
          </Margin>
          <ExperienceSection>
            <ExperienceDate>2021 — 2019</ExperienceDate>
            <ExperienceOccupation>BA, Design & Business</ExperienceOccupation>
            <ExperienceOrganization>
              KEA - Københavns erhvervsakademi
            </ExperienceOrganization>
          </ExperienceSection>
          <ExperienceSection>
            <ExperienceDate>2019 — 2017</ExperienceDate>
            <ExperienceOccupation>
              AP, Multimedia design & Communication
            </ExperienceOccupation>
            <ExperienceOrganization>
              KEA - Københavns erhvervsakademi
            </ExperienceOrganization>
          </ExperienceSection>
          <ExperienceSection>
            <ExperienceDate>2014 — 2010</ExperienceDate>
            <ExperienceOccupation>Multimedia design</ExperienceOccupation>
            <ExperienceOrganization>
              Secondary school of arts
            </ExperienceOrganization>
          </ExperienceSection>
        </Experiences>
      </Margin>
      <Photography />
    </Container>
  </Margin>
);

const Container = styled(ContainerWrapper)``;

export default Home;
