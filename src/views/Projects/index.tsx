import React, { useEffect } from 'react';
import { useParams, Redirect } from 'react-router-dom';
import { Flex } from '../../components/FlexBox';
import MaxWidthContainer from '../../atoms/MaxWidthContainer';
import Suztain from './Suztain';
import ProjectCollection from './ProjectCollection';
import Rockcommerce from './RockcommerceDesignSystem';
import Whyser from './Whyser';

export const Projects = () => {
  const { id } = useParams<{ id: string }>();

  useEffect(() => {
    const htmlElement = document.querySelector('html');

    if (htmlElement) {
      htmlElement.scrollTop = 0;
    }
  }, []);

  if (id === '01') {
    return (
      <Flex flexDirection="column" justifyContent="start" alignItems="center">
        <MaxWidthContainer maxWidth={893}>
          <Whyser />
        </MaxWidthContainer>
      </Flex>
    );
  }

  if (id === '02') {
    return (
      <Flex flexDirection="column" justifyContent="start" alignItems="center">
        <MaxWidthContainer maxWidth={893}>
          <Suztain />
        </MaxWidthContainer>
      </Flex>
    );
  }

  if (id === '03') {
    return (
      <Flex flexDirection="column" justifyContent="start" alignItems="center">
        <MaxWidthContainer maxWidth={893}>
          <ProjectCollection />
        </MaxWidthContainer>
      </Flex>
    );
  }

  if (id === '04') {
    return (
      <Flex flexDirection="column" justifyContent="start" alignItems="center">
        <MaxWidthContainer maxWidth={893}>
          <Rockcommerce />
        </MaxWidthContainer>
      </Flex>
    );
  }

  return <Redirect to="/" />;
};
