import React from "react";
import { ProjectHeadingPrimary } from "../../atoms/ProjectHeadings";
import Zoom from "react-medium-image-zoom";
import Margin from "../../atoms/Margin";
import Padding from "../../atoms/Padding";
import Paragraph from "../../atoms/Paragraph";
import { Link } from "react-router-dom";
import { Flex } from "../../components/FlexBox";
import closeIconSvg from "../../icons/closeIcon.svg";
import rockommerceImg1 from "../../img/Rockwool/rockcommerce1.jpg";
import rockommerceImg2 from "../../img/Rockwool/rockcommerce2.jpg";
import rockommerceImg3 from "../../img/Rockwool/rockcommerce3.jpg";
import rockcommerce2Img from "../../img/Rockwool/Image_collage1.jpg";
import { ProjectNavigation } from "../../components/ProjectNavigation/ProjectNavigation";
import MaxWidthContainer from "../../atoms/MaxWidthContainer";

const RockcommerceDesignSystem = () => (
  <Margin top={80} bottom={80}>
    <Padding left={20} right={20}>
      <Flex flexDirection="row" justifyContent="space-between">
        <Margin bottom={30}>
          <ProjectHeadingPrimary>
            <ProjectHeadingPrimary>
              Rockcommerce design system
            </ProjectHeadingPrimary>
          </ProjectHeadingPrimary>
        </Margin>
        <Link to="/">
          <img src={closeIconSvg} alt="close-icon" />
        </Link>
      </Flex>
      <Margin top={10}>
        <MaxWidthContainer maxWidth={740}>
          <Paragraph>
            B2B platform for ROCKWOOL Group, the world leader in stone wool
            solutions and other customized products for industrial applications.
            Rockwool’s insulation products are used during the construction of
            buildings to provide energy-saving, fire-proofing, and noise
            reduction.
          </Paragraph>
        </MaxWidthContainer>
      </Margin>
      <Margin top={30} bottom={120}>
        <Flex flexDirection="row">
          <div style={{ marginRight: "15px" }}>
            <Zoom>
              <img width={"100%"} src={rockommerceImg1} />
            </Zoom>
          </div>
          <div style={{ marginLeft: "15px" }}>
            <Flex
              style={{ height: "100%" }}
              flexDirection="column"
              justifyContent="space-between"
            >
              <Zoom>
                <img width={"100%"} src={rockommerceImg2} />
              </Zoom>
              <Zoom>
                <img width={"100%"} src={rockommerceImg3} />
              </Zoom>
            </Flex>
          </div>
        </Flex>
      </Margin>
      <Margin top={20}>
        <MaxWidthContainer maxWidth={740}>
          <Paragraph>
            As a part of Vertic, I’ve been working on design system for
            Rockwool’s e-commerce solution which is supporting multi-country,
            multi-currency, multi-brand management with complex sales channels
            and will be rolling out in 39 countries. During the project I’ve
            been part of the team working with agile method, delivering the work
            through the sprints. My role in this project was a co-designer, in
            the final stages I took over the full responsibility of the project
            and delivering final sprints on time.
          </Paragraph>
        </MaxWidthContainer>
      </Margin>
      <Margin top={30} bottom={120}>
        <Zoom>
          <img width={"100%"} src={rockcommerce2Img} />
        </Zoom>
      </Margin>
      <ProjectNavigation />
    </Padding>
  </Margin>
);

export default RockcommerceDesignSystem;
