import React from "react";
import { ProjectHeadingPrimary } from "../../atoms/ProjectHeadings";
import Margin from "../../atoms/Margin";
import Paragraph from "../../atoms/Paragraph";
import Zoom from "react-medium-image-zoom";
import { ProjectNavigation } from "../../components/ProjectNavigation/ProjectNavigation";
import Padding from "../../atoms/Padding";
import closeIconSvg from "../../icons/closeIcon.svg";
import { Flex } from "../../components/FlexBox";
import { Link } from "react-router-dom";
import clothioImg from "../../img/clothio.png";
import editorialLayoutImg from "../../img/editorial-layout.png";
import highPawImg from "../../img/Image_highpaw.png";
import uiChallangeImg from "../../img/ui-challenge.png";
import MaxWidthContainer from "../../atoms/MaxWidthContainer";

const ProjectCollection = () => (
  <Margin top={80} bottom={80}>
    <Padding left={20} right={20}>
      <Margin bottom={10}>
        <Flex flexDirection="row" justifyContent="space-between">
          <Margin bottom={30}>
            <ProjectHeadingPrimary>
              <ProjectHeadingPrimary>App Concept</ProjectHeadingPrimary>
            </ProjectHeadingPrimary>
          </Margin>
          <Link to="/">
            <img src={closeIconSvg} alt="close-icon" />
          </Link>
        </Flex>
      </Margin>
      <Margin top={10}>
        <MaxWidthContainer maxWidth={740}>
          <Paragraph>
            The objective of the project was to create a realistic startup
            concept with a service solution within lifestyle and fashion.
            Clothio is a clothing tracking app using a sustainable rating system
            to give each product an easy-to-understand score. The platform will
            inform the users about product quality, materials used,
            environmental impact, and sustainability score based on 3 core
            values (social good, planet-friendly, and animal cruelty-free).
          </Paragraph>
        </MaxWidthContainer>
      </Margin>
      <Margin top={30}>
        <Zoom>
          <img width={"100%"} src={clothioImg} />
        </Zoom>
      </Margin>

      <Margin top={130} bottom={20}>
        <ProjectHeadingPrimary>
          High Paw visual identity and web design
        </ProjectHeadingPrimary>
      </Margin>
      <Margin bottom={20}>
        <MaxWidthContainer maxWidth={740}>
          <Paragraph>
            High Paw! is a small company providing professional pet sitting
            around Copenhagen. The task was to find a company in need of
            improving its digital presence. Our collaboration was based on
            creating a new visual language and a better user experience. I
            created an e-commerce solution with a multi-step sign-up flow and
            booking process. Users can now book a service directly from the
            dashboard and are able to choose from available days and times, and
            make payment online. This solution has been created to simplify,
            originally complicated sign-up and booking process.
          </Paragraph>
        </MaxWidthContainer>
      </Margin>
      <Margin top={30}>
        <Zoom>
          <img width={"100%"} src={highPawImg} />
        </Zoom>
      </Margin>
      <Margin top={130}>
        <ProjectHeadingPrimary>Daily UI Challenge</ProjectHeadingPrimary>
      </Margin>
      <Margin top={20}>
        <MaxWidthContainer maxWidth={740}>
          <Paragraph>
            Daily UI is a series of daily design challenges, which help
            designers to practice, improve and experiment with visual styles
            without a lot of limitation.
          </Paragraph>
        </MaxWidthContainer>
      </Margin>
      <Margin top={30} bottom={160}>
        <Zoom>
          <img width={"100%"} src={uiChallangeImg} />
        </Zoom>
      </Margin>
      <Margin top={130} bottom={20}>
        <ProjectHeadingPrimary>
          <ProjectHeadingPrimary>Editorial layout</ProjectHeadingPrimary>
        </ProjectHeadingPrimary>
      </Margin>
      <Margin top={20} bottom={30}>
        <MaxWidthContainer maxWidth={740}>
          <Paragraph>
            Indie magazine concept, inspired by russian constructivism, bauhaus,
            swiss design and vintage prints.
          </Paragraph>
        </MaxWidthContainer>
      </Margin>

      <Margin bottom={120}>
        <Zoom>
          <img width={"100%"} src={editorialLayoutImg} />
        </Zoom>
      </Margin>

      <ProjectNavigation />
    </Padding>
  </Margin>
);

export default ProjectCollection;
