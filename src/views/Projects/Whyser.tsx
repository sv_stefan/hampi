import React from "react";
import styled from "styled-components";
import {
  ProjectHeadingPrimary,
  ProjectHeadingTertiary,
} from "../../atoms/ProjectHeadings";
import Margin from "../../atoms/Margin";
import Padding from "../../atoms/Padding";
import Paragraph from "../../atoms/Paragraph";
import { Link } from "react-router-dom";
import { Flex } from "../../components/FlexBox";
import closeIconSvg from "../../icons/closeIcon.svg";
import whyserImg1 from "../../img/corporate-identity/Image_overview.jpg";
import whyserImg2 from "../../img/corporate-identity/Image_designprocess.jpg";
import whyserImg3 from "../../img/corporate-identity/Image_brand.jpg";
import whyserImg4 from "../../img/corporate-identity/Image_logo.jpg";
import whyserImg5 from "../../img/corporate-identity/Image_colors.jpg";
import whyserImg6 from "../../img/corporate-identity/Image_typography.png";
import whyserImg7 from "../../img/corporate-identity/Image_icons.jpg";
import whyserImg8 from "../../img/corporate-identity/Image_web.jpg";
import whyserImg9 from "../../img/corporate-identity/Image_app.jpg";
import whyserImg10 from "../../img/corporate-identity/Image_brandbook.jpg";
import { ProjectNavigation } from "../../components/ProjectNavigation/ProjectNavigation";
import MaxWidthContainer from "../../atoms/MaxWidthContainer";
import Zoom from "react-medium-image-zoom";
import "react-medium-image-zoom/dist/styles.css";

const VerticalAlignContainer = styled.div<{ reversed?: boolean }>`
  display: flex;
  flex-direction: column;

  @media screen and (min-width: 930px) {
    align-items: center;
    flex-direction: ${({ reversed }) => (reversed ? "row-reverse" : "row")};
  }
`;

const ResponsiveMargin = styled(Margin)`
  @media screen and (max-width: 930px) {
    margin-left: 0;
    margin-right: 0;
  }
`;

const ResponsiveMaxWidthContainer = styled(MaxWidthContainer)`
  @media screen and (max-width: 930px) {
    max-width: 100%;
  }
`;

const Whyser = () => (
  <Margin top={80} bottom={80}>
    <Padding left={20} right={20}>
      <Margin bottom={10}>
        <Flex flexDirection="row" justifyContent="space-between">
          <Margin bottom={30}>
            <ProjectHeadingPrimary>
              Corporate identity for Whyser
            </ProjectHeadingPrimary>
          </Margin>
          <Link to="/">
            <img src={closeIconSvg} alt="close-icon" />
          </Link>
        </Flex>
      </Margin>
      <ResponsiveMaxWidthContainer maxWidth={740}>
        <Margin top={10}>
          <Paragraph>
            This project was developed as part of my bachelor thesis, in which I
            have developed a brand strategy and visual identity for the case
            company Whyser. The company offers an online SaaS platform that has
            been designed to help leaders manage business strategies together
            with their organization. The application enables users to develop
            transparent and agile strategies and allows all employees to
            collaborate and contribute.
          </Paragraph>
        </Margin>
        <Margin top={20}>
          <Paragraph>
            The purpose of the new visual identity is to attract attention,
            create recognition, and differentiate it from well-established
            competitors. New visual language manifests itself through a bold
            logo, dynamic layouts, bright color palette, distinctive gradients,
            and bold typography with friendly quality.
          </Paragraph>
        </Margin>
      </ResponsiveMaxWidthContainer>
      <Margin top={20}>
        <Zoom>
          <img src={whyserImg1} width="100%" />
        </Zoom>
      </Margin>
      <Margin top={120}>
        <ProjectHeadingTertiary>Design process</ProjectHeadingTertiary>
        <ResponsiveMaxWidthContainer maxWidth={740}>
          <Margin top={10}>
            <Paragraph>
              To ensure the successful delivery of the solution, I have used the
              double diamond model for mapping out the design processes. The
              project started with the Discovery phase, in which a problem was
              identified, using qualitative and quantitative research methods.
              Afterward, findings from the discovery phase were analyzed, using
              various methods and theories, resulting in a clear definition of
              the problem to be addressed through a design-led solution. During
              the Development phase, the prototype was created, tested, and
              iterated. Lastly, the final design solution was designed in detail
              using creative techniques, which addressed the needs identified in
              the Discovery phase.
            </Paragraph>
          </Margin>
        </ResponsiveMaxWidthContainer>
        <Margin top={50}>
          <Zoom>
            <img src={whyserImg2} width="100%" />
          </Zoom>
        </Margin>
      </Margin>
      <Margin top={120}>
        <MaxWidthContainer maxWidth={890}>
          <VerticalAlignContainer>
            <MaxWidthContainer maxWidth={552}>
              <Zoom>
                <img src={whyserImg3} width="100%" />
              </Zoom>
            </MaxWidthContainer>
            <ResponsiveMargin left={74} top={10}>
              <ResponsiveMaxWidthContainer maxWidth={265}>
                <Margin bottom={10}>
                  <ProjectHeadingTertiary>
                    Defining the brand
                  </ProjectHeadingTertiary>
                </Margin>
                <Paragraph>
                  Whyser is a friendly and approachable brand that evokes a
                  feeling that they are modern and fresh - something that people
                  want to be part of. If Whyser would be a person, it would be a
                  female in her forties that does not take things too seriously.
                  She is someone who brings people together by making them feel
                  included and valued.
                </Paragraph>
              </ResponsiveMaxWidthContainer>
            </ResponsiveMargin>
          </VerticalAlignContainer>
        </MaxWidthContainer>
      </Margin>
      <Margin top={120}>
        <Margin bottom={10}>
          <ProjectHeadingTertiary>The logo</ProjectHeadingTertiary>
        </Margin>
        <ResponsiveMaxWidthContainer maxWidth={445}>
          <Margin top={10}>
            <Paragraph>
              The primary logo is made up of two parts, a logomark, and a
              wordmark. It’s available in 2 versions: a dynamic logo with
              gradient and small shadow; and a flat logo which consists of solid
              colors only. Logo fits well with the company's personality and the
              tech industry they operate in.
            </Paragraph>
          </Margin>
        </ResponsiveMaxWidthContainer>
        <Margin top={50}>
          <Zoom>
            <img src={whyserImg4} width="100%" />
          </Zoom>
        </Margin>
      </Margin>
      <Margin top={120}>
        <MaxWidthContainer maxWidth={890}>
          <VerticalAlignContainer reversed>
            <MaxWidthContainer maxWidth={450}>
              <Zoom>
                <img src={whyserImg5} width="100%" />
              </Zoom>
            </MaxWidthContainer>
            <ResponsiveMargin right={68} top={10}>
              <ResponsiveMaxWidthContainer maxWidth={371}>
                <Margin bottom={10}>
                  <ProjectHeadingTertiary>Colors</ProjectHeadingTertiary>
                </Margin>

                <Paragraph>
                  The color palette consists of deep blues that complement the
                  pale pink and aquamarine while helping those to stand out and
                  create a contrast. Blues are colors of trust and loyalty and
                  have a calming and relaxing effect, the light pink is the
                  color of compassion and femininity and brings warmth to the
                  color palette. Aquamarine color brings freshness and energy.
                </Paragraph>
              </ResponsiveMaxWidthContainer>
            </ResponsiveMargin>
          </VerticalAlignContainer>
        </MaxWidthContainer>
      </Margin>
      <Margin top={120}>
        <MaxWidthContainer maxWidth={890}>
          <VerticalAlignContainer>
            <ResponsiveMaxWidthContainer maxWidth={450}>
              <Zoom>
                <img src={whyserImg6} width="100%" />
              </Zoom>
            </ResponsiveMaxWidthContainer>
            <ResponsiveMargin left={89} top={10}>
              <ResponsiveMaxWidthContainer maxWidth={364}>
                <Margin bottom={10}>
                  <ProjectHeadingTertiary>Typography</ProjectHeadingTertiary>
                </Margin>
                <Paragraph>
                  The primary typeface is Davis Sans, an airy, open, and modern
                  typeface that ensures optimal readability. From a function
                  standpoint, as a sans serif font, it is legible in both large
                  and small sizes and several weights, which lends versatility
                  to the design. Our primary typeface for any web or print
                  materials is Davis Sans. This clean sans serif typeface best
                  represents the modern feel of the brand.
                </Paragraph>
              </ResponsiveMaxWidthContainer>
            </ResponsiveMargin>
          </VerticalAlignContainer>
        </MaxWidthContainer>
      </Margin>
      <Margin top={120}>
        <Margin bottom={10}>
          <ProjectHeadingTertiary>Iconography</ProjectHeadingTertiary>
        </Margin>
        <MaxWidthContainer maxWidth={890}>
          <Zoom>
            <img src={whyserImg7} width="100%" />
          </Zoom>
        </MaxWidthContainer>
      </Margin>
      <Margin top={120}>
        <ProjectHeadingTertiary>Website</ProjectHeadingTertiary>
        <MaxWidthContainer maxWidth={740}>
          <Margin top={10}>
            <Paragraph>
              Brand elements working together, forming a recognizable and
              memorable brand identity.
            </Paragraph>
          </Margin>
        </MaxWidthContainer>
        <Margin top={30}>
          <Zoom>
            <img src={whyserImg8} width="100%" />
          </Zoom>
        </Margin>
      </Margin>
      <Margin top={120}>
        <ProjectHeadingTertiary>Software application</ProjectHeadingTertiary>
        <MaxWidthContainer maxWidth={740}>
          <Margin top={10}>
            <Paragraph>
              The visual identity translates into the software application
              slightly differently, since the user interface has to firstly
              emphasize software functionality and demonstrate respect for the
              user. The visual language has been toned down, but still keeps
              distinctive brand elements in a subtle way.
            </Paragraph>
          </Margin>
        </MaxWidthContainer>
        <Margin top={30}>
          <Zoom>
            <img src={whyserImg9} width="100%" />
          </Zoom>
        </Margin>
      </Margin>
      <Margin top={120} bottom={120}>
        <ProjectHeadingTertiary>Brand guidelines</ProjectHeadingTertiary>
        <MaxWidthContainer maxWidth={740}></MaxWidthContainer>
        <Margin top={30}>
          <Zoom>
            <img src={whyserImg10} width="100%" />
          </Zoom>
        </Margin>
      </Margin>

      <ProjectNavigation />
    </Padding>
  </Margin>
);

export default Whyser;
