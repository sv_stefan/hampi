import React from "react";
import Zoom from "react-medium-image-zoom";
import {
  ProjectHeadingPrimary,
  ProjectHeadingTertiary,
} from "../../atoms/ProjectHeadings";
import Margin from "../../atoms/Margin";
import Paragraph from "../../atoms/Paragraph";
import suztainGif from "../../img/Suztain_Logo_animation.gif";
import suztainImg01 from "../../img/suztain01.png";
import suztainImg02 from "../../img/suztain02.jpg";
import suztainImg03 from "../../img/suztain03.png";
import suztainImg04 from "../../img/suztain04.jpg";
import { ProjectNavigation } from "../../components/ProjectNavigation/ProjectNavigation";
import Padding from "../../atoms/Padding";
import closeIconSvg from "../../icons/closeIcon.svg";
import { Flex } from "../../components/FlexBox";
import { Link } from "react-router-dom";
import MaxWidthContainer from "../../atoms/MaxWidthContainer";

const Suztain = () => (
  <Margin top={80} bottom={80}>
    <Padding left={20} right={20}>
      <Margin bottom={10}>
        <Flex flexDirection="row" justifyContent="space-between">
          <Margin bottom={30}>
            <ProjectHeadingPrimary>
              <ProjectHeadingPrimary>
                Visual identity for Suztain
              </ProjectHeadingPrimary>
            </ProjectHeadingPrimary>
          </Margin>
          <Link to="/">
            <img src={closeIconSvg} alt="close-icon" />
          </Link>
        </Flex>
      </Margin>
      <MaxWidthContainer maxWidth={740}>
        <Margin top={10}>
          <Paragraph>
            Suztain is an online retailer in Denmark with a goal, to offer their
            customers a selection of sustainable products to help keep their
            environmental footprint as small as possible. The purpose of this
            project was to identify and analyze the business challenges that an
            organization is faced with and based on that develop a brand
            strategy. The problem found in the analysis is that Suztain’s brand
            image is outdated and doesn’t communicate the character of a Nordic
            company.
          </Paragraph>
        </Margin>
        <Margin top={10} bottom={40}>
          <Paragraph>
            A solution proposed is a new visual language for the brand. The
            inspiration for Suztain is natural and feminine, with an earthy and
            Scandinavian aesthetic. The result is a minimalist brand image with
            a Nordic touch. This project has been developed as part of a school
            assignment. The company has been pleased with the solution and has
            decided to implement it.
          </Paragraph>
        </Margin>
      </MaxWidthContainer>
      <Zoom>
        <img width={"100%"} src={suztainImg01} />
      </Zoom>
      <Margin top={130} bottom={50}>
        <Margin bottom={12}>
          <ProjectHeadingTertiary>Logo design</ProjectHeadingTertiary>
        </Margin>
        <MaxWidthContainer maxWidth={740}>
          <Paragraph>
            Suztain’s primary logo is a combination of a wordmark, brandmark,
            and tagline. The lettering style has a modern and feminine vibe and
            gives an impression of harmony and roundness. Brandmark stands for a
            leaf detail showing ribs. Suztain is organized around the idea of
            inspiring people to take action towards a sustainable lifestyle.
            Tagline ‘take a small step’ represents this value and encourages, to
            take small steps to live more sustainably.
          </Paragraph>
        </MaxWidthContainer>
      </Margin>
      <Margin bottom={100}>
        <Zoom>
          <img width={"100%"} src={suztainGif} />
        </Zoom>
        <Zoom>
          <img width={"100%"} src={suztainImg02} />
        </Zoom>
      </Margin>
      <Margin top={130} bottom={50}>
        <ProjectHeadingTertiary>Brand Book</ProjectHeadingTertiary>
      </Margin>
      <Margin bottom={50}>
        <Zoom>
          <img width={"100%"} src={suztainImg03} />
        </Zoom>
      </Margin>
      <Margin bottom={160}>
        <Zoom>
          <img width={"100%"} src={suztainImg04} />
        </Zoom>
      </Margin>
      <ProjectNavigation />
    </Padding>
  </Margin>
);

export default Suztain;
