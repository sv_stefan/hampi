import styled from 'styled-components';
import defaultFont from '../styled/fonts';

export const ProjectHeadingPrimary = styled.h2`
  font-family: "Bodoni Moda", sans-serif;
  font-size: 40px;
  font-weight: 600;
  letter-spacing: 0;
  line-height: 50px;
  color: #180643;

  @media screen and (min-width: 1200px) {
    font-size: 45px;
    line-height: 50px;
  }
`;

export const ProjectHeadingSecondary = styled.h2`
  ${defaultFont};

  font-size: 30px;
  font-weight: 600;
  letter-spacing: 0;
  line-height: 50px;
  color: #180643;

  @media screen and (min-width: 1200px) {
    font-size: 35px;
    line-height: 39px;
  }
`;

export const ProjectHeadingTertiary = styled.h3`
  ${defaultFont};

  font-size: 22px;
  letter-spacing: 0;
  color: #180643;

  @media screen and (min-width: 1200px) {
    font-size: 28px;
  }
`;
