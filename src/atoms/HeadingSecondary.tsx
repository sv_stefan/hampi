import React, { ReactNode } from 'react';
import styled from 'styled-components';
import defaultFont from '../styled/fonts';

const HeadingSecondary = ({ children, color = '#180643'} : { children: ReactNode, color?: string}) => <StyledHeading color={color}>{children}</StyledHeading>;

const StyledHeading = styled.h2`
  ${defaultFont}
  font-weight: 600;
  font-size: 20px;
  color: ${({ color }) => color};
  letter-spacing: 0;
  line-height: 28px;
`;

export default HeadingSecondary;
