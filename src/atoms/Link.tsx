import { Link as NavLink } from 'react-router-dom';
import styled, { css } from 'styled-components';
import defaultFonts from '../styled/fonts';

const LinkCSS = css`
  ${defaultFonts};
  font-size: 16px;
  color: #ff8962;
  letter-spacing: 0;
  line-height: 23px;
`;

export const Link = styled.a`
  ${LinkCSS};

  display: flex;
  align-items: center;
  cursor: pointer;
  text-decoration: none;
`;

export const DecoratedLink = styled(NavLink)`
  ${LinkCSS};
`;

export const ExternalLink = styled.a`
  ${LinkCSS};

  font-size: 14px;
`;

export const BrandLogoLink = styled(NavLink)`
  ${defaultFonts};

  font-size: 16px;
  font-weight: 700;
  color: #180643;
  letter-spacing: 0;
  line-height: 21px;
  text-decoration: none;
`;

export const UnstyledLink = styled(NavLink)`
  text-decoration: none;
  color: #180643;
  outline: 0;
`;
