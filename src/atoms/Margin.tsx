import React, { ReactNode } from 'react';
import styled from 'styled-components';

interface IMargin extends IStyledMargin {
  children: ReactNode;
}

interface IStyledMargin {
  top?: number;
  bottom?: number;
  left?: number;
  right?: number;
}

const Margin = ({ children, ...rest }: IMargin) => <StyledMargin {...rest}>{children}</StyledMargin>;

const StyledMargin = styled.div`
  margin-top: ${(props: IStyledMargin): number => (props.top ? props.top : 0)}px;
  margin-bottom: ${(props: IStyledMargin): number => (props.bottom ? props.bottom : 0)}px;
  margin-left: ${(props: IStyledMargin): number => (props.left ? props.left : 0)}px;
  margin-right: ${(props: IStyledMargin): number => (props.right ? props.right : 0)}px;
`;

export default Margin;
