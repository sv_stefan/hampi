import React, { FC } from "react";
import styled from "styled-components";

const HeadingPrimary: FC<{ fontAlign: "left" | "right" }> = ({
  children,
  fontAlign,
}) => <StyledHeading fontAlign={fontAlign}>{children}</StyledHeading>;

const StyledHeading = styled.h1<{ fontAlign: "left" | "right" }>`
  font-family: "Bodoni Moda", sans-serif;
  font-size: 47px;
  line-height: 47px;
  color: #180643;
  letter-spacing: 0;
  margin: 0 0 29px 0;
  font-weight: 600;
  text-align: ${({ fontAlign }) => fontAlign};

  @media screen and (min-width: 930px) {
    font-size: 65px;
    line-height: 65px;
  }
`;

export default HeadingPrimary;
