import styled from 'styled-components';

const MaxWidthContainer = styled.div`
  width: 100%;
  max-width: ${({ maxWidth }: { maxWidth: number }): number => (maxWidth ? maxWidth : 0)}px;
`;

export default MaxWidthContainer;
