import React, { ReactNode } from 'react';
import styled from 'styled-components';
import defaultFont from '../styled/fonts';

const Paragraph = ({ children, color = '#180643' }: { children: ReactNode, color?: string }) => <StyledParagraph color={color}>{children}</StyledParagraph>;

const StyledParagraph = styled.p`
  ${defaultFont};
  font-size: 14px;
  color: ${({ color }) => color};
  letter-spacing: 0;
  line-height: 22px;
`;

export default Paragraph;
