import React, { ReactNode } from 'react';
import styled from 'styled-components';

interface IPadding extends IStyledPadding {
  children: ReactNode;
}

interface IStyledPadding {
  top?: number;
  bottom?: number;
  left?: number;
  right?: number;
}

const Padding = ({ children, ...rest }: IPadding) => <StyledPadding {...rest}>{children}</StyledPadding>;

const StyledPadding = styled.div`
  padding-top: ${(props: IStyledPadding): number => (props.top ? props.top : 0)}px;
  padding-bottom: ${(props: IStyledPadding): number => (props.bottom ? props.bottom : 0)}px;
  padding-left: ${(props: IStyledPadding): number => (props.left ? props.left : 0)}px;
  padding-right: ${(props: IStyledPadding): number => (props.right ? props.right : 0)}px;
`;

export default Padding;
