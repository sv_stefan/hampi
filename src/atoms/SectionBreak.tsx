import styled from 'styled-components';

const SectionBreak = styled.hr`
  border-top: 1px solid #180643;
  width: 100%;
`;

export default SectionBreak;
