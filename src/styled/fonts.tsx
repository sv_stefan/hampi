import { css } from 'styled-components';

const fonts = css`
  font-family: 'Jost', sans-serif;
`;

export default fonts;
