import styled from 'styled-components';

export const ContainerWrapper = styled.div`
  margin: 0 auto;
  max-width: 890px;
`;
