import React, { ReactNode, FC } from "react";
import { withRouter, RouteComponentProps } from "react-router-dom";
import styled from "styled-components";
import { useParams } from "react-router-dom";
import { ProjectHeadingTertiary } from "../../atoms/ProjectHeadings";
import defaultFont from "../../styled/fonts";
import { Flex } from "../FlexBox";
import arrowRightInBubbleIcon from "../../icons/arrowRightInBubbleIcon.svg";
import Margin from "../../atoms/Margin";
import MaxWidthContainer from '../../atoms/MaxWidthContainer';

const ProjectNav: FC<RouteComponentProps> = ({ history }) => {
  const { id } = useParams<{ id: string }>();

  if (!id) {
    return null;
  }

  const handleRouteChange = (projectNumber: string) =>
    history.push(`/projects/${projectNumber}`);

  const handlePrompt = (projectNumber: string) => {
    handleRouteChange(projectNumber);
  };

  return (
    <StyledNavigation>
      <Margin bottom={24}>
        <ProjectHeadingTertiary>View projects</ProjectHeadingTertiary>
      </Margin>
      <LinksContainer>
        {id !== "01" && (
          <Link
            onClick={() => handlePrompt("01")}
            contentText="UI design, Web design, Brand guidelines, Visual identity"
          >
            Corporate identity system
          </Link>
        )}
        {id !== "02" && (
          <Link
            onClick={() => handlePrompt("02")}
            contentText="Visual identity, Brand guidelines, Web design"
          >
            Visual identity
          </Link>
        )}
        {id !== "03" && (
          <Link
            onClick={() => handlePrompt("03")}
            contentText="Web Design, Visual Identity, Editorial Layout, Logo design, Mobile App"
          >
            Collection of smaller projects
          </Link>
        )}
        {id !== "04" && (
          <Link onClick={() => handlePrompt("04")} contentText="Design system">
            Rockcommerce design system
          </Link>
        )}
      </LinksContainer>
    </StyledNavigation>
  );
};

const StyledNavigation = styled.div``;

const LinksContainer = styled.div``;

const StyledLink = styled.a`
  cursor: pointer;
  display: flex;
  padding: 20px 6px;
  border-top: 1px solid #180643;
  border-bottom: 1px solid #180643;
  margin: -1px 0 0 0;
  text-decoration: none;
`;

const StyledSpan = styled.span`
  ${defaultFont};

  font-size: 16px;
  color: #ff8962;
  letter-spacing: 0;
  line-height: 23px;
`;

const StyledContentText = styled.p`
  ${defaultFont};

  font-size: 14px;
  line-height: 22px;
  letter-spacing: 0;
  color: #180643;
`;

const Link = ({
  children,
  contentText,
  onClick,
}: {
  contentText: string;
  children: ReactNode;
  onClick: () => void;
}) => (
  <StyledLink onClick={onClick}>
    <Flex justifyContent="space-between" alignItems="center">
      <MaxWidthContainer maxWidth={300}>
        <Flex alignItems="center">
          <StyledSpan>{children}</StyledSpan>
        </Flex>
      </MaxWidthContainer>
      <Flex
        flexDirection="row"
        alignItems="center"
        justifyContent="space-between"
      >
        <StyledContentText>{contentText}</StyledContentText>
        <img alt="arrow-right-icon" src={arrowRightInBubbleIcon} />
      </Flex>
    </Flex>
  </StyledLink>
);

export const ProjectNavigation = withRouter(ProjectNav);
