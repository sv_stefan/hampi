import React from 'react';
import styled from 'styled-components';
import defaultFont from '../../styled/fonts';
import arrowIcon from '../../icons/arrowIcon.svg';
import { Link } from '../../atoms/Link';
import { withRouter, RouteComponentProps } from 'react-router-dom';

interface IProps extends RouteComponentProps {
  projectNumber: string;
  title: string;
  buttonName: string;
  text?: string;
  margin: string;
}

const ProjectCard = ({ projectNumber, title, text, margin, history, buttonName }: IProps) => {
  const handleRouteChange = () => history.push(`/projects/${projectNumber}`);

  return (
    <StyledProject style={{ margin }}>
      <StyledProjectNumber>—{projectNumber}</StyledProjectNumber>
      <StyledTitle>{title}</StyledTitle>
      <StyledText>{text}</StyledText>
      <Link onClick={handleRouteChange}>
        {buttonName}
        <img alt="arrow-icon" src={arrowIcon} style={{ marginLeft: '8px' }} />
      </Link>
    </StyledProject>
  );
};

const StyledProject = styled.div`
  padding: 25px 0;
  border-top: 1px solid #180643;
  border-bottom: 1px solid #180643;
`;

const StyledProjectNumber = styled.div`
  ${defaultFont}
  font-size: 30px;
  color: #180643;
  letter-spacing: 0;
  line-height: 30px;
  margin: 0 0 8px 0;
`;

const StyledTitle = styled.div`
  ${defaultFont}
  font-size: 20px;
  color: #180643;
  letter-spacing: 0;
  line-height: 28px;
  margin: 0 0 4px 0;
  font-weight: 500;
`;

const StyledText = styled.div`
  ${defaultFont}
  font-size: 14px;
  color: #180643;
  letter-spacing: 0;
  line-height: 22px;
  margin: 0 0 16px 0;
`;

export default withRouter(ProjectCard);
