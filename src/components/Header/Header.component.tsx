import React, { useState } from 'react';
import styled from 'styled-components';
import defaultFont from '../../styled/fonts';
import { BrandLogoLink } from '../../atoms/Link';
import { Flex } from '../FlexBox';
import openMobileMenuIcon from '../../icons/menu.svg';
import closeMobileMenuIcon from '../../icons/closeIcon.svg';
import Margin from '../../atoms/Margin';
import Padding from '../../atoms/Padding';

const Header = () => (
  <HeaderContainer>
    <BrandLogoLink to="/">M.H. Portfolio</BrandLogoLink>
    <HeaderSide>—2021</HeaderSide>
    <MobileMenu />
  </HeaderContainer>
);

const MobileMenu = () => {
  const [isMobileMenuOpen, setMobileMenuVisibility] = useState<boolean>(false);
  const handleMobileMenuVisibility = () => setMobileMenuVisibility((prevValue: boolean) => !prevValue);

  const handleMenuOnClick = (event: any) => {
    event.preventDefault();
    const link = event.currentTarget.getAttribute('href');

    handleMobileMenuVisibility();
    document.querySelector(link).scrollIntoView({ behavior: 'smooth', block: 'start' });
  };

  return (
    <MobileMenuStyled>
      <MobileMenuButton onClick={handleMobileMenuVisibility}>
        <img src={openMobileMenuIcon} alt="open-mobile-menu-icon" />
      </MobileMenuButton>
      <MenuOverlay menuOpened={isMobileMenuOpen}>
        <Flex justifyContent="flex-end">
          <Margin top={27} right={20}>
            <MobileMenuButton onClick={handleMobileMenuVisibility}>
              <img src={closeMobileMenuIcon} alt="close-mobile-menu-icon" />
            </MobileMenuButton>
          </Margin>
        </Flex>
        <Margin top={27}>
          <Padding left={20} right={20}>
            <Flex flexDirection="column">
              <MobileMenuLink href="#home" onClick={handleMenuOnClick}>
                .Home
              </MobileMenuLink>
              <MobileMenuLink href="#projects" onClick={handleMenuOnClick}>
                .Projects
              </MobileMenuLink>
              <MobileMenuLink href="#about" onClick={handleMenuOnClick}>
                .About
              </MobileMenuLink>
              <MobileMenuLink href="#contact" onClick={handleMenuOnClick}>
                .Contact
              </MobileMenuLink>
            </Flex>
          </Padding>
        </Margin>
      </MenuOverlay>
    </MobileMenuStyled>
  );
};

const MobileMenuLink = styled.a`
  ${defaultFont};

  text-decoration: none;
  font-size: 14px;
  color: #180643;
  letter-spacing: 0;
  line-height: 26px;
  margin: -1px 0 0 0;
  padding: 17px;
  border-top: 1px solid #180643;
  border-bottom: 1px solid #180643;
`;

const HeaderContainer = styled.div`
  ${defaultFont}
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  max-width: 890px;
  padding: 27px 0 42px 0;
  margin: 0 0 120px 0;

  @media screen and (min-width: 930px) {
    margin: 0 auto;
  }
`;

const HeaderSide = styled.div`
  display: none;

  @media screen and (min-width: 1200px) {
    display: block;
    ${defaultFont}
    transform: rotate(-360deg);
    font-size: 50px;
    color: #180643;
    letter-spacing: 0;
    line-height: 50px;
    margin: 0 -160px 0 0;
  }
`;

const MobileMenuStyled = styled.div`
  display: block;

  @media screen and (min-width: 930px) {
    display: none;
  }
`;

const MobileMenuButton = styled.div`
  cursor: pointer;
`;

const MenuOverlay = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  visibility: ${({ menuOpened }: { menuOpened: boolean }) => (menuOpened ? 'visible' : 'hidden')};
  opacity: ${({ menuOpened }: { menuOpened: boolean }) => (menuOpened ? 1 : 0)};
  transition: all 250ms;
  background: #fff;
`;

export default Header;
