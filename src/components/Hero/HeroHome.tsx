import React, { useEffect } from "react";
import styled from "styled-components";
import { useIntersectionObserver } from "../../reusable/useIntersectionObserver";
import { useInView } from "react-intersection-observer";
import defaultFont from "../../styled/fonts";
import HeadingPrimary from "../../atoms/HeadinePrimary";
import MaxWidthContainer from '../../atoms/MaxWidthContainer';

const HeroHome = () => {
  const [ref, inView] = useInView({
    threshold: 1,
  });

  const { setIntersection } = useIntersectionObserver();

  useEffect(() => {
    if (inView) {
      setIntersection("#home");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [inView]);

  return (
    <div ref={ref}>
      <Wrapper>
        <HeadingPrimary fontAlign="left">
          I’m a digital designer based in Copenhagen
        </HeadingPrimary>
        <RightAlignedContent>
          <MaxWidthContainer maxWidth={450}>
            <Paragraph>
              Hi! I’m Michaela, a digital designer specializing in product
              design & visual identities. I have graduated from Copenhagen
              School of Design & Technology with a degree in Design & Business.
            </Paragraph>
          </MaxWidthContainer>
        </RightAlignedContent>
      </Wrapper>
    </div>
  );
};

const Wrapper = styled.div`
  max-width: 660px;
`;

const RightAlignedContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`;

const Paragraph = styled.p`
  ${defaultFont}
  font-size: 16px;
  color: #180643;
  letter-spacing: 0;
  line-height: 26px;
  margin: 0 0 50px 0;

  a {
    color: #180643;
  }
`;

export default HeroHome;
