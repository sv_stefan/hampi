import React, { ReactNode } from "react";
import styled from "styled-components";

const FlexBox = styled.div`
  display: flex;
`;

const StyledFlexRow = styled(FlexBox)`
  flex-direction: row;
`;

const StyledFlexColumn = styled(FlexBox)`
  flex-direction: column;
`;

const StyledFlexEnd = styled(FlexBox)`
  align-items: flex-end;
`;

export const FlexRow = ({ children }: { children: ReactNode }) => (
  <StyledFlexRow>{children}</StyledFlexRow>
);
export const FlexColumn = ({ children }: { children: ReactNode }) => (
  <StyledFlexColumn>{children}</StyledFlexColumn>
);
export const FlexEnd = ({ children }: { children: ReactNode }) => (
  <StyledFlexEnd>{children}</StyledFlexEnd>
);

interface IProps {
  style?: any;
  children: ReactNode;
  flexDirection?: "column" | "row" | "column-reverse" | "row-reverse";
  justifyContent?:
    | "center"
    | "start"
    | "end"
    | "flex-start"
    | "flex-end"
    | "left"
    | "right"
    | "space-around"
    | "space-between";
  alignItems?: "center" | "start" | "end" | "flex-start" | "flex-end";
}

const FlexBoxContainer = styled.div<IProps>` 
  ${({ style }) => ({ ...style })};

  width: 100%;
  display: flex;
  ${(props) => props.flexDirection && "flex-direction:" + props.flexDirection};
  ${(props) =>
    props.justifyContent && "justify-content:" + props.justifyContent};
  ${(props) => props.alignItems && "align-items:" + props.alignItems};
`;

export const Flex = (props: IProps) => <FlexBoxContainer {...props} />;
