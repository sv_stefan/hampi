import React, { FC } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Home from './views/Home/Home.view';
import Header from './components/Header/Header.component';
import styled from 'styled-components';
import defaultFont from './styled/fonts';
import { useIntersectionObserver } from './reusable/useIntersectionObserver';
import { Projects } from './views/Projects';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import Contact from './views/Home/Contact';

interface IProps {}

const App: FC<IProps> = () => {
  return (
    <Router>
      <Route
        render={({ location }) => (
          <TransitionGroup>
            <CSSTransition key={location.key} classNames="fade" timeout={450}>
              <Switch location={location}>
                <Route path="/projects/:id">
                  <Projects />
                </Route>
                <Route path="/">
                  <HomeContainer id="home">
                    <Header />
                    <NavigationBar />
                    <Home />
                  </HomeContainer>
                  <Contact />
                </Route>
                <Route path="/projects/*">
                  <Redirect from="*" to="/" />
                </Route>
              </Switch>
            </CSSTransition>
          </TransitionGroup>
        )}
      />
    </Router>
  );
};

const HomeContainer = styled.div`
  padding: 0 20px;

  @media screen and (min-width: 930px) {
    padding: 0;
  }
`;

const StyledNavigation = styled.nav`
  display: none;

  @media screen and (min-width: 1200px) {
    display: block;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translateX(-570px);
  }
`;

const StyledUnorderedList = styled.ul`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const StyledList = styled.li`
  list-style: none;
`;

const StyledLink = styled.a`
  ${defaultFont}
  display: flex;
  flex-direction: row;
  align-items: center;
  position: relative;
  text-decoration: none;
  font-size: 14px;
  color: #30302f;
  letter-spacing: 0;
  line-height: 26px;
  transition: font-weight 250ms;

  &.active {
    font-weight: 700;

    div {
      display: block;
    }
  }
`;

const StyledLine = styled.div`
  display: none;
  position: absolute;
  width: 47px;
  margin-left: -50px;
  border: 1px solid #30302f;
  margin-right: 10px;
`;

const NavigationBar = () => {
  const { currentIntersection } = useIntersectionObserver();

  const handleActiveLink = (event: any) => {
    event.preventDefault();
    const link = event.currentTarget.getAttribute('href');

    document.querySelector(link).scrollIntoView({ behavior: 'smooth', block: 'start' });
  };

  return (
    <StyledNavigation>
      <StyledUnorderedList>
        <StyledList>
          <StyledLink
            href="#home"
            className={currentIntersection === '#home' ? 'active' : ''}
            onClick={handleActiveLink}
          >
            <StyledLine />
            <span>.Home</span>
          </StyledLink>
        </StyledList>
        <StyledList>
          <StyledLink
            href="#projects"
            className={currentIntersection === '#projects' ? 'active' : ''}
            onClick={handleActiveLink}
          >
            <StyledLine />
            <span>.Projects</span>
          </StyledLink>
        </StyledList>
        <StyledList>
          <StyledLink
            href="#about"
            className={currentIntersection === '#about' ? 'active' : ''}
            onClick={handleActiveLink}
          >
            <StyledLine />
            <span>.About</span>
          </StyledLink>
        </StyledList>
        <StyledList>
          <StyledLink
            href="#contact"
            className={currentIntersection === '#contact' ? 'active' : ''}
            onClick={handleActiveLink}
          >
            <StyledLine />
            <span>.Contact</span>
          </StyledLink>
        </StyledList>
      </StyledUnorderedList>
    </StyledNavigation>
  );
};

export default App;
