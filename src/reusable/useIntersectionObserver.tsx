import { useState } from 'react';
import { createStore } from 'reusable';

export const useIntersectionObserver = createStore(() => {
  const [currentIntersection, setIntersection] = useState('');

  return {
    currentIntersection,
    setIntersection,
  };
});
